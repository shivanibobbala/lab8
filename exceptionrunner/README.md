1. The log to console is limited to warning messages only, whereas the other log (to file) contains all messages. 
2. From Tester.java. The @Disabled annotation is used to disable JUnit Tests.
3. It checks to see if the expected exception was thrown. In this case, TimerException.
4. 
	1. The serialVersionUID is a version identifier for a serializable class. We need it because this number is used during deserialization to ensure that a loaded class corresponds exactly to a serialized object. 
	2. In order to create a custom exception, we extended the Exception base class. We need to override the constructors so that detail message and cause are specified. 
	3. We don't need the other constructors, and there aren't any other methods that we would need to override to successfully implement the TimerException. 
5. The static block configures the logger according to the properties specified in file "logger.properties"
6. md stands for Markdown which is the syntax that these files are written in. README files will be displayed by bitbucket on the repository's source page. The fine can contain Markdown and a restricted set of HTML tags.
7.  Test is failing because a TimerException is expected but a null pointer exception is thrown. In Timer, the timeMe method needs
to be fixed so that none of the values will produce a null pointer exception.
8. The problem is that the null pointer exception was being thrown in the finally 
block, which is the last thing to execute regardless of whether exceptions were thrown or not. This is why the 
JUnit test is failing. 
9. Screenshot of IntelliJ (not Eclipse) JUnit Tests included. 
10. Screenshot of Maven test run included
11. NullPointerException is a RunTime Exception. TimerException is a Checked exception and has to be declared by the programmer. 
12. Code pushed to repository. 